import kotlinx.coroutines.runBlocking
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import util.Hello
import util.get
import util.post

fun main (args: Array<String>) {
    println("Hello World!")

    // Try adding program arguments via Run/Debug configuration.
    // Learn more about running applications: https://www.jetbrains.com/help/idea/running-applications.html.
    println("Program arguments: ${args.joinToString()}")
    runBlocking{
        post("http://192.168.50.2:8080/api/v1/GPSVz2oBr6CzGZvOa1mA/telemetry",
                Hello(3.0f)
        )
    }
}

