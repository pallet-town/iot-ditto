package util

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable

/**
 * HTTP 工具类
 * 〈功能介绍〉
 * @author 许通
 * @version 2024/2/10
 */
object Http {
}

suspend fun get(url: String) {
    val client = HttpClient(CIO);
    val response: HttpResponse = client.get(url)
    val responseBody: ByteArray = response.body()

    println(responseBody)
    client.close()
}

suspend fun post(
    url: String, payload: PostPayload
): Int {
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json()
        }
    }
    val message = client.post(url) {
        contentType(ContentType.Application.Json)
        setBody(payload)
    }
    client.close()

    return when (message.status.value) {
        in 200..209 -> {
            println("ok")
            1
        }

        else -> {
            println("error")
            1
        }
    }


}

sealed class PostPayload

@Serializable
data class Hello(val temperature: Float) : PostPayload()

/**
 * curl -v -X POST http://localhost:8080/api/v1/GPSVz2oBr6CzGZvOa1mA/telemetry --header Content-Type:application/json --data "{temperature:25}"
 */