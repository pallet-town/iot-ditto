package device

import java.time.Instant.now

/**
 * 〈一句话功能简述〉
 * 〈功能介绍〉
 * @author 许通
 * @version 2024/2/126
 */
class DataPoint {
    var valueByTimeFunc: (Float)->Float = {t->0f}
    var samplePeriodSecond:Int =0;
    var startTimestamp=now();
}